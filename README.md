# Fast Dice

Reading, calculating and printing formatted dice roll sums using C11 threads and POSIX function calls.
