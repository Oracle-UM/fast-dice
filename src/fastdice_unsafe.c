#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <threads.h>
#include <time.h>

#define MIN_MT_FILESIZE 1000

int process_segment(void *ptr) {
    unsigned long rand_next = clock() + time(NULL);
    char *input_ptr = (char *)ptr;
    char *output_ptr = (char *)ptr;

    while (*input_ptr) {
        // read rolls
        int rolls = 0;
        while (isdigit(*input_ptr))
            rolls = rolls * 10 + (*input_ptr++ - '0');

        // skip 'd'
        input_ptr++;

        // read faces
        int faces = 0;
        while (isdigit(*input_ptr))
            faces = faces * 10 + (*input_ptr++ - '0');
        if (*input_ptr)
            input_ptr++;

        // calc sums using short rand()
        int sum = rolls;
        while (rolls--) {
            rand_next = rand_next * 1103515245 + 12345;
            sum += (((unsigned int)(rand_next / 65536) % 32768)) % faces;
        }

        // print sums to buffer
        int i;
        char tmp[20];  // store sums
        for (i = 0; sum; sum /= 10)
            tmp[i++] = sum % 10 + '0';
        while (i--)
            *output_ptr++ = tmp[i];
        *output_ptr++ = '\n';
    }
    *output_ptr = 0;

    return 0;
}

int main(void) {
    FILE *input_file = fopen("input.txt", "r");

    // get file size
    fseek(input_file, 0, SEEK_END);
    unsigned long file_size = (unsigned long)ftell(input_file);
    rewind(input_file);

    // read file to buffer
    char *buffer = malloc(file_size + 1);
    fread(buffer, 1, file_size, input_file);
    fclose(input_file);
    buffer[file_size] = 0;

    // split buffer by threads
    unsigned short num_threads = get_nprocs_conf();
    if (file_size < MIN_MT_FILESIZE)
        num_threads = 1;
    char *input_ptrs[num_threads];
    input_ptrs[0] = buffer;
    for (int i = 1; i < num_threads; i++) {
        input_ptrs[i] = buffer + (file_size / num_threads) * i;
        while (*input_ptrs[i] != '\n')
            input_ptrs[i]++;
        *input_ptrs[i] = 0;
        input_ptrs[i]++;
    }

    // process segments in parallel and print results
    FILE *output_file = fopen("output.txt", "w");
    thrd_t thread_id[num_threads];
    for (int i = 0; i < num_threads; ++i)
        thrd_create(thread_id + i, process_segment, (void *)(input_ptrs[i]));
    for (int i = 0; i < num_threads; ++i) {
        thrd_join(thread_id[i], NULL);
        fputs(input_ptrs[i], output_file);
    }
    fclose(output_file);
    free(buffer);

    return 0;
}
