#include "wrappers.h"
#include "fastdice.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

void *malloc_wrapper(const size_t size) {
    void *ptr = malloc(size);

    if (!ptr) {
        fprintf(stderr, "Memory allocation failed: error code %d ('%s')\n",
                errno, strerror(errno));
        exit(ErrOutOfMemory);
    }

    return ptr;
}

FILE *fopen_wrapper(const char *const __restrict__ filename,
                    const char *const __restrict__ modes) {
    FILE *const fp = fopen(filename, modes);

    if (!fp) {
        fprintf(stderr, "File '%s' open failed: error code %d ('%s')\n",
                filename, errno, strerror(errno));
        exit(ErrFileNull);
    }

    if (!strchr(modes, 'w') && get_file_size(fp) == 0) {
        fprintf(stderr, "File '%s' is empty\n", filename);
        exit(ErrFileEmpty);
    }

    return fp;
}

void fseek_wrapper(FILE *const stream, const long off, const int whence) {
    if (fseek(stream, off, whence) == -1) {
        fprintf(stderr, "File seek failed: error code %d ('%s')\n", errno,
                strerror(errno));
        exit(ErrFileSeek);
    }
}

long ftell_wrapper(FILE *const stream) {
    const long file_pos = ftell(stream);

    if (-1 == file_pos) {
        fprintf(stderr, "File tell failed: error code %d ('%s')\n", errno,
                strerror(errno));
        exit(ErrFileTell);
    }

    return file_pos;
}

void fread_wrapper(void *const __restrict__ ptr,
                   const size_t size,
                   const size_t nmemb,
                   FILE *const __restrict__ stream) {
    if (fread(ptr, size, nmemb, stream) != nmemb) {
        fprintf(stderr, "File read failed: error code %d ('%s')\n", errno,
                strerror(errno));
        exit(ErrFileRead);
    }
}

void fclose_wrapper(FILE *const stream) {
    if (fclose(stream) == EOF) {
        fprintf(stderr, "File close failed: error code %d ('%s')\n", errno,
                strerror(errno));
        exit(ErrFileClose);
    }
}
