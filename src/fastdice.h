#ifndef FASTDICE
#define FASTDICE

#include <stdio.h>

size_t get_file_size(FILE *fp);

#endif
