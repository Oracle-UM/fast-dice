#ifndef WRAPPERS
#define WRAPPERS

#include <stdio.h>

enum ErrCodes {
    ErrInvArgs = -1,
    ErrOutOfMemory = -2,
    ErrUnexpChar = -3,
    ErrFileNull = -4,
    ErrFileEmpty = -5,
    ErrFileSeek = -6,
    ErrFileRead = -7,
    ErrFileTell = -8,
    ErrFileClose = -9
};

void *malloc_wrapper(size_t size);

FILE *fopen_wrapper(const char *__restrict__ filename,
                    const char *__restrict__ modes);

void fseek_wrapper(FILE *stream, long off, int whence);

long ftell_wrapper(FILE *stream);

void fread_wrapper(void *__restrict__ ptr,
                   size_t size,
                   size_t nmemb,
                   FILE *__restrict__ stream);

void fclose_wrapper(FILE *stream);

#endif
