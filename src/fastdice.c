#include "fastdice.h"
#include "wrappers.h"

#include <ctype.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <threads.h>
#include <unistd.h>

/* min file size for multithreading */
#define MIN_MT_FILESIZE 1000

inline static void get_answer(void) {
AWAIT_ANSWER:
    switch (tolower(fgetc(stdin))) {
        case 'y':
            break;
        case 'n':
            puts("Execution aborted.");
            exit(EXIT_FAILURE);
            break;
        default:
            goto AWAIT_ANSWER;
            break;
    }
}

size_t get_file_size(FILE *const fp) {
    fseek_wrapper(fp, 0, SEEK_END);
    const size_t size = ftell_wrapper(fp);
    rewind(fp);
    return size;
}

static char **split_buffer(char *const buffer,
                           const size_t size,
                           const thrd_t nthreads) {
    char **const input_ptrs = malloc_wrapper(nthreads * sizeof(char *));
    input_ptrs[0] = buffer;

    for (thrd_t tid = 1; tid < nthreads; ++input_ptrs[tid++]) {
        input_ptrs[tid] = buffer + (size / nthreads) * tid;
        while (*input_ptrs[tid] != '\n')
            ++input_ptrs[tid];
        *input_ptrs[tid] = '\0';
    }

    return input_ptrs;
}

static int thread_segment(void *const arg) {
    uint_fast64_t rand_next = clock() + time(NULL);
    char *input_ptr = arg, *output_ptr = arg;

    while (*input_ptr) {
        if (!isdigit(*input_ptr)) {
            fprintf(stderr, "File read failed: unexpected character '%c'\n",
                    *input_ptr);
            exit(ErrUnexpChar);
        }

        /* read rolls */
        uint_fast16_t rolls = 0;
        while (isdigit(*input_ptr))
            rolls = rolls * 10 + (*input_ptr++ - '0');

        if ('d' != *input_ptr) {
            fprintf(stderr, "File read failed: unexpected character '%c'\n",
                    *input_ptr);
            exit(ErrUnexpChar);
        }

        /* skip 'd' */
        ++input_ptr;

        if (!isdigit(*input_ptr)) {
            fprintf(stderr, "File read failed: unexpected character '%c'\n",
                    *input_ptr);
            exit(ErrUnexpChar);
        }

        /* read faces */
        uint_fast8_t faces = 0;
        while (isdigit(*input_ptr))
            faces = faces * 10 + (*input_ptr++ - '0');

        /* skip '\n' */
        if ('\n' == *input_ptr)
            ++input_ptr;
        else if ('\0' != *input_ptr) {
            fprintf(stderr, "File read failed: unexpected character '%c'\n",
                    *input_ptr);
            exit(ErrUnexpChar);
        }

        /* calc sums using short rand */
        uint_fast64_t sum = rolls;
        while (rolls--) {
            rand_next = rand_next * 1103515245 + 12345;
            sum += ((rand_next / 65536) % 32768) % faces;
        }

        /* print sums to buffer */
        uint_fast8_t i;
        char tmp[20];
        for (i = 0; sum != 0; sum /= 10)
            tmp[i++] = sum % 10 + '0';
        while (i--)
            *output_ptr++ = tmp[i];
        *output_ptr++ = '\n';
    }
    *output_ptr = '\0';

    return EXIT_SUCCESS;
}

static void start_mt(char *const buffer,
                     const size_t file_size,
                     FILE *const output) {
    const thrd_t nthreads = file_size > MIN_MT_FILESIZE ? get_nprocs_conf() : 1;
    char **const input_ptrs = split_buffer(buffer, file_size, nthreads);
    thrd_t thread_ids[nthreads];

    for (thrd_t tid = 0; tid < nthreads; ++tid)
        thrd_create(thread_ids + tid, thread_segment, input_ptrs[tid]);
    for (thrd_t tid = 0; tid < nthreads; ++tid) {
        thrd_join(thread_ids[tid], NULL);
        fputs(input_ptrs[tid], output);
    }

    free(input_ptrs);
}

int main(int argc, char *argv[]) {
    if (3 != argc) {
        fprintf(stderr, "Invalid parameters.\n%s [input file] [output file]\n",
                argv[0]);
        exit(ErrInvArgs);
    }

    if (access(argv[2], F_OK) == 0) {
        printf("File '%s' exists. Overwrite? [y/n]\n", argv[2]);
        get_answer();
    }

    FILE *const input = fopen_wrapper(argv[1], "r");
    const size_t file_size = get_file_size(input);
    char *const buffer = malloc_wrapper(file_size + 1);
    fread_wrapper(buffer, file_size, 1, input);
    buffer[file_size] = '\0';
    fclose_wrapper(input);

    FILE *const output = fopen_wrapper(argv[2], "w");
    start_mt(buffer, file_size, output);

    fclose_wrapper(output);
    free(buffer);

    return EXIT_SUCCESS;
}
